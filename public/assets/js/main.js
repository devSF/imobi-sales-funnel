$(function() {

    /*$('#nb-prj').change(function ()
    {
        let nb = $(this).val();
        if(nb == 1)
        {
            $('#support').html('3 000 DHS');
            $('#heberg').html('800 DHS');
        }
        else if(nb == 2)
        {
            $('#support').html('5 000 DHS');
            $('#heberg').html('800 DHS');
        }
        else if(nb == 3)
        {
            $('#support').html('6 500 DHS');
            $('#heberg').html('1100 DHS');
        }
        else if(nb >= 4)
        {
            $('#support').html('7 500 DHS');
            $('#heberg').html('1700 DHS');
        }

        if(nb > 0)
        {
            $('#price').html('25 000 DHS');
            $('#price-rs').css('display', 'block');
            $('#price-choice').css('display', 'block');

        }
        else
        {
            $('#price-rs').css('display', 'none');
            $('#price-choice').css('display', 'none');
        }
    });


    $('#send-ct').click(function () {
        gtag_report_conversion("{{ path('imm_home_contact') }}")
    })

    $('#get-demo').click(function(){
        $('#play').trigger('click');
    });*/

    if($(window).width()<768)
    {
        $('#intro').css('margin-top', '5rem');
        $('.gradient-mask').css('margin-left', '10px');
    }

    $('#contact-us').click(function(){
        $('#contact-title').html("Besoin de plus d'infos ?<br>Nous serons ravis de vous répondre.");
        $('#contact-phone, #contact-address').css('display', 'block');
        $('#demo-request').html('Votre demande: Plus d\'infos, Démonstration détaillée, etc.');
        $('#demo-request').val('Votre demande: Plus d\'infos, Démonstration détaillée, etc.');
    });

});

function setOffer(ch)
{
    if(ch === 1)
    {
        $('#demo-request').html('Je suis intéressé par une démo.');
        $('#demo-request').val('Je suis intéressé par une démo.');
        $('#contact-title').html('Merci pour votre intérêt.<br>Veuillez saisir vos informations de contact.')
        //$('#contact-phone, #contact-address').css('display', 'none');
    }

    $.post('https://www.imobi.ma/experience', {ch:ch});
}