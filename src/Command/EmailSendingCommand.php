<?php

namespace App\Command;

use App\Service\EmailMarketing;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class EmailSendingCommand extends Command
{
    protected static $defaultName = 'app:email-sending';
    private $emailMarketing;

    public function __construct(EmailMarketing $emailMarketing, string $name = null)
    {
        parent::__construct($name);
        $this->emailMarketing = $emailMarketing;
    }

    protected function configure()
    {
        $this->setDescription('Automatisation envoi email marketing');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->emailMarketing->send();

        return Command::SUCCESS;
    }
}
