<?php


namespace App\Service;


use App\Entity\Order;
use App\Entity\User;
use App\Notification\OrderConfirmationNotification;
use App\Notification\OrderCreationNotification;
use App\Notification\OrderValidationNotification;
use App\Notification\ProspectRegistrationNotification;
use App\Notification\ProspectReviewNotification;
use App\Notification\ProspectValidationNotification;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Notifier\Recipient\Recipient;
use Twig\Environment;

class EmailManager
{

    private $mailer;
    private $twig;

    public function __construct(\Swift_Mailer $mailer, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function send(
        string $type,
        string $subject,
        User $user,
        Order $order=null
    )
    {
        $template="";
        $emailTo = $user->getEmail();
        $emailFrom = ['support@imobi.ma' => 'Support - Imobi'];

        switch ($type) {
            case 'prospect-review' :
                $template = 'prospect_review';
                $emailTo = 'imobi.maroc@gmail.com';
                break;
            case 'prospect-validation' :
                $template = 'prospect_validation';
                break;
            case 'order-creation' :
                $template = 'order_creation';
                $emailFrom = ['sales@imobi.ma' => 'Sales - Imobi'];
                $emailTo = 'imobi.maroc@gmail.com';
                break;
            case 'order-confirmation' :
                $template = 'order_confirmation';
                $emailFrom = ['sales@imobi.ma' => 'Sales - Imobi'];
                break;
           case 'order-validation' :
                $template = 'order_validation';
                $emailFrom = ['sales@imobi.ma' => 'Sales - Imobi'];
                break;
        }

        $message = (new \Swift_Message($subject))
            ->setFrom($emailFrom)
            ->addTo($emailTo)
        ;

        $html = $this->twig->render(
            'emails/'.$template.'_notification.html.twig',
            [
                'customer' => $user,
                'order' => $order,
            ]
        );
        $message->setBody($html, 'text/html');

        try {
            $this->mailer->send($message);
        }
        catch (TransportExceptionInterface $e) {}
    }

}