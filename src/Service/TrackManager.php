<?php


namespace App\Service;


use App\Service\GeoPlugin;
use App\Entity\Tracking;
use Doctrine\ORM\EntityManagerInterface;

class TrackManager
{
    private $entityManager;
    private $geoPlugin;

    public function __construct(
        EntityManagerInterface $entityManager,
        GeoPlugin $geoPlugin
    )
    {
        $this->entityManager = $entityManager;
        $this->geoPlugin = $geoPlugin;
    }

    function isMobileDevice() {
        return preg_match(
            "/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|".
            "mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i",
            $_SERVER["HTTP_USER_AGENT"]
        );
    }

    public function new($type, $name, $user=null)
    {
        $this->geoPlugin->locate();
        if ('Oregon' != $this->geoPlugin->region) {
            $track = new Tracking();
            $track->setType($type);
            $track->setName($name);
            $track->setUser($user);
            $track->setRegion($this->geoPlugin->region);
            $track->setCity($this->geoPlugin->city);
            $track->setIp($this->geoPlugin->ip);
            $track->setIsMobile($this->isMobileDevice());

            $this->entityManager->persist($track);
            $this->entityManager->flush();
        }
    }

    public function get(int $type, $timeType, $times, $timesMonth, $timesYear, $ip)
    {
        $cond="WHERE t.name <> 'login'";
        $query="";
        $params=[];

        if(isset($ip) && $ip != "")
        {
            $cond.=" AND t.ip =:ip";
            $params['ip'] = $ip;
        }
        if($type == 1)  // Tracking List
        {
            $query = "SELECT t.name, count(t.id) AS count
             FROM App\Entity\Tracking t 
             $cond    
             GROUP BY t.name
             ORDER BY t.name ASC
            ";
        }

        return
            $this
                ->entityManager
                ->createQuery($query)
                ->setParameters($params)
                ->getResult()
            ;

    }
}