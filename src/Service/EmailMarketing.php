<?php /** @noinspection PhpUnhandledExceptionInspection */


namespace App\Service;


use App\Entity\Sending;
use App\Repository\SendingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\TwigBundle\DependencyInjection\TwigExtension;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Notifier\Recipient\Recipient;
use Twig\Environment;

class EmailMarketing
{

    private $mailer;
    private $twig;
    private $sendingRepository;
    private $entityManager;

    public function __construct(
        \Swift_Mailer $mailer,
        Environment $twig,
        SendingRepository $sendingRepository,
        EntityManagerInterface $entityManager
    )
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->sendingRepository = $sendingRepository;
        $this->entityManager = $entityManager;
    }

    public function send()
    {
        $emailsToSend = $this->sendingRepository->getEmailsToSend();

        foreach ($emailsToSend as $emailToSend)
        {
            $user = $emailToSend->getUser();
            $email = $emailToSend->getEmail();

            $message = (new \Swift_Message(
                str_replace(
                    '{username}',
                    $user->getPseudo(),
                    $email->getSubject()
                )
            ))
                ->setFrom(['abdelilah@imobi.ma' => 'Abdelilah - Imobi'])
                ->addTo($user->getEmail())
                ->setMaxLineLength(1000)
            ;
            $html = $this
                ->twig
                ->render(
                    'marketing/content.html.twig',
                    [
                        'user' => $user,
                        'content' => $email->getContent()
                    ]
                )
            ;
            $message->setBody($html, 'text/html');

            try {
                $this->mailer->send($message);
                $emailToSend->setIsSent(1);

                if($next = $email->getNext())
                {
                    $sending = new Sending();
                    $sending->setUser($user);
                    $sending->setEmail($next);
                    $sending->setSentAt(
                        (new \DateTime())->add(new \DateInterval('P7D'))
                    );
                    $this->entityManager->persist($sending);
                }
                $this->entityManager->flush();

            }
            catch (TransportExceptionInterface $e) {}
        }

    }
}