<?php

namespace App\Repository;

use App\Entity\Sending;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Sending|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sending|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sending[]    findAll()
 * @method Sending[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SendingRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sending::class);
    }

    public function getEmailsToSend()
    {
        return
            $this
                ->createQueryBuilder('s')
                ->andWhere('s.isSent IS NULL OR s.isSent = 0')
                ->andWhere('s.sentAt <= :date')
                ->setParameters([
                    'date' => new \DateTime(),
                    ]
                )
                ->getQuery()
                ->getResult()
            ;
    }
}
