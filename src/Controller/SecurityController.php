<?php

namespace App\Controller;

use App\Service\TrackManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @param Session $session
     * @param TrackManager $trackManager
     * @return Response
     */
    public function login(
        AuthenticationUtils $authenticationUtils,
        Session $session,
        TrackManager $trackManager
    ): Response
    {
        if ($this->getUser()) {
            $session->remove('must-register');
            if(!$current = $session->get('current')) {
                return $this->redirectToRoute(
                    'demo',
                    ['slug' => 'introduction']
                );
            }
            return $this->redirectToRoute(
                'demo',
                ['slug' => $current->getSlug()]
            );
        }
        $trackManager->new('auth', 'login', $this->getUser());

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'security/login.html.twig',
            ['last_username' => $lastUsername, 'error' => $error]
        );
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
