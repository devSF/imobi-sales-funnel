<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Service\EmailManager;
use App\Service\TrackManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     * @param Request $request
     * @param Session $session
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param EmailManager $emailManager
     * @param TrackManager $trackManager
     * @return Response
     */
    public function register(
        Request $request,
        Session $session,
        UserPasswordEncoderInterface $passwordEncoder,
        EmailManager $emailManager,
        TrackManager $trackManager
    ): Response
    {
        if ($this->getUser()) {
            if(!$current = $session->get('current')) {
                $current = 'introduction';
            }
            return $this->redirectToRoute(
                'demo',
                ['slug' => $current]
            );
        }
        $trackManager->new('auth', 'register');

        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $user->setRoles(['ROLE_USER']);
            $user->setType('prospect');
            $user->setState('submitted');

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $emailManager->send(
                'prospect-review',
                'Nouveau Prospect Intéressé par Une Démo',
                $user
            );

            $session->getFlashBag()->add(
                'success',
                'Nous avons bien reçu votre inscription. 
                 Vous recevrez un email de confirmation sur votre boîte '.$user->getEmail().' une fois elle est validée.'
            );


            $session->set('register', 1);

            return $this->redirectToRoute('register_confirmed');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/registration-confirmed", name="register_confirmed")
     *
     * @param Session $session
     * @return Response
     */
    public function confirmed(Session $session)
    {
        if(!$session->get('register'))
        {
            return $this->redirectToRoute('app_login');
        }
        $session->remove('register');

        return $this->render('registration/confirmed.html.twig', []);
    }
}
