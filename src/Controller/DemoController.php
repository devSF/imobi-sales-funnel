<?php /** @noinspection PhpUnhandledExceptionInspection */

namespace App\Controller;

use App\Entity\Tracking;
use App\Entity\Video;
use App\Repository\SectionRepository;
use App\Service\TrackManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class DemoController extends AbstractController
{
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * +
     * @Route("/demo_header", name="demo_header")
     * +
     * @param SectionRepository $sectionRepository
     * @return Response
     *
     */
    public function demoHeader(SectionRepository $sectionRepository): Response
    {
        return new Response(
            $this->twig->render('demo/header.html.twig', [
                'sections' => $sectionRepository->findAll(),
                ])
        );
    }

    /**
     * @Route("/", defaults={"slug": "introduction"})
     * @Route("/demo/{slug}", name="demo", defaults={"slug": "introduction"})
     * @param Video $video
     * @param Session $session
     * @param TrackManager $trackManager
     * @return Response
     */
    public function view(
        Video $video,
        Session $session,
        TrackManager $trackManager
    )
    : Response
    {
        $session->set('current', $video);
        $user = $this->getUser();
        if(
            ($user && !in_array('ROLE_ADMIN', $user->getRoles())) || !$user
        ) {
            $trackManager->new('demo', $video->getSlug(), $user);
        }
        if($video->getIsPrivate() && !$user)
        {
            $session->set('must-register', 1);
            return $this->redirectToRoute('app_login');
        }
        return new Response(
            $this->twig->render('demo/view.html.twig', [
                'current' => $video
            ])
        );
    }
}
