<?php

namespace App\Controller;

use App\Repository\VideoRepository;
use App\Service\TrackManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AnalyticsController extends AbstractController
{
    /**
     * @Route("/analytics", name="analytics")
     * @param Request $request
     * @param TrackManager $trackManager
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function index(
        Request $request,
        TrackManager $trackManager,
        EntityManagerInterface $entityManager
    ): Response
    {
        $times = array_reverse($this->getTimesInt(1));
        $timesString = array_reverse($this->getTimesString(1));
        $timesYear = array_reverse($this->getTimesYears(1));
        $timesMonth = array_reverse($this->getTimesMonths(1));
        $timesMonthString = array_reverse($this->getTimesMonthsString(1));
        $ip = $request->get('ip');
        $cond="";
        $params = [];

        if(isset($ip) && $ip != "")
        {
            $cond=" AND t.ip =:ip";
            $params['ip'] = $ip;
        }
        $actions = $entityManager
            ->createQuery(
                "SELECT DISTINCT t.name
                FROM App\Entity\Tracking t 
                WHERE t.name <> 'login'
                $cond
                ORDER BY t.name ASC"
            )
            ->setParameters($params)
            ->getScalarResult()
        ;
        $ips = $entityManager
            ->createQuery(
                "SELECT DISTINCT t.ip
                FROM App\Entity\Tracking t 
                WHERE t.name <> 'login'
                AND t.ip IS NOT NULL
                AND t.ip <> ''"
            )
            ->getScalarResult()
        ;
        $tracks = $trackManager->get(
            1,
            1,
            $times,
            $timesMonth,
            $timesYear,
            $request->get('ip')
        );

        return $this->render('analytics/index.html.twig', [
            'times' => $timesString,
            'actions' => $actions,
            'ips' => $ips,
            'ipFilter' => $ip,
            'tracks' => $tracks,
            'graph' => 'Total Actions'
        ]);
    }

    public function getTimesInt($type)
    {
        if($type == 1)      // 7 derniers jours
        {
            $days = array();
            for ($i = 1; $i <= 7; $i++)
            {
                $days[] = date("d", strtotime( date( 'Y-m-d' )." -$i days"));
            }
            return $days;
        }
        elseif($type == 2)      // 30 derniers jours
        {
            $days = array();
            for ($i = 1; $i <= 30; $i++)
            {
                $days[] = date("d", strtotime( date( 'Y-m-d' )." -$i days"));
            }
            return $days;
        }
        elseif($type == 5)      // 90 derniers jours
        {
            $days = array();
            for ($i = 1; $i <= 90; $i++)
            {
                $days[] = date("d", strtotime( date( 'Y-m-d' )." -$i days"));
            }
            return $days;
        }
        elseif($type == 6)      // 6 derniers mois
        {
            $months = array();
            for ($i = 1; $i <= 6; $i++)
            {
                $months[] = date("m", strtotime( date( 'Y-m-01' )." -$i months"));
            }
            return $months;
        }
        elseif($type == 3)      // 12 derniers mois
        {
            $months = array();
            for ($i = 1; $i <= 12; $i++)
            {
                $months[] = date("m", strtotime( date( 'Y-m-01' )." -$i months"));
            }
            return $months;
        }
        elseif($type == 4)      // 24 derniers mois
        {
            $months = array();
            for ($i = 1; $i <= 24; $i++)
            {
                $months[] = date("m", strtotime( date( 'Y-m-01' )." -$i months"));
            }
            return $months;
        }
    }

    public function getTimesString($type)
    {
        if($type == 1)      // 7 derniers jours
        {
            $days = array();
            for ($i = 1; $i <= 7; $i++)
            {
                $days[] = date("d", strtotime( date( 'Y-m-d' )." -$i days"));
            }
            return $days;
        }
        elseif($type == 2)      // 30 derniers jours
        {
            $days = array();
            for ($i = 1; $i <= 30; $i++)
            {
                $days[] = date("d", strtotime( date( 'Y-m-d' )." -$i days"));
            }
            return $days;
        }
        elseif($type == 5)      // 90 derniers jours
        {
            $days = array();
            for ($i = 1; $i <= 90; $i++)
            {
                $days[] = date("d", strtotime( date( 'Y-m-d' )." -$i days"));
            }
            return $days;
        }
        elseif($type == 6)      // 6 derniers mois
        {
            $months = array();
            for ($i = 1; $i <= 6; $i++)
            {
                $months[] = date("M", strtotime( date( 'Y-m-01' )." -$i months"));
            }
            return $months;
        }
        elseif($type == 3)      // 12 derniers mois
        {
            $months = array();
            for ($i = 1; $i <= 12; $i++)
            {
                $months[] = date("M", strtotime( date( 'Y-m-01' )." -$i months"));
            }
            return $months;
        }
        elseif($type == 4)      // 24 derniers mois
        {
            $months = array();
            for ($i = 1; $i <= 24; $i++)
            {
                $months[] = date("M", strtotime( date( 'Y-m-01' )." -$i months"));
            }
            return $months;
        }
    }

    public function getTimesMonths($type)
    {
        if($type == 1)      // 7 derniers jours
        {
            $months = array();
            for ($i = 1; $i <= 7; $i++)
            {
                $months[] = date("m", strtotime( date( 'Y-m-d' )." -$i days"));
            }
            return $months;
        }
        elseif($type == 2)      // 30 derniers jours
        {
            $months = array();
            for ($i = 1; $i <= 30; $i++)
            {
                $months[] = date("m", strtotime( date( 'Y-m-d' )." -$i days"));
            }
            return $months;
        }
        elseif($type == 5)      // 90 derniers jours
        {
            $months = array();
            for ($i = 1; $i <= 90; $i++)
            {
                $months[] = date("m", strtotime( date( 'Y-m-d' )." -$i days"));
            }
            return $months;
        }

    }

    public function getTimesMonthsString($type)
    {
        if($type == 1)      // 7 derniers jours
        {
            $months = array();
            for ($i = 1; $i <= 7; $i++)
            {
                $months[] = date("M", strtotime( date( 'Y-m-d' )." -$i days"));
            }
            return $months;
        }
        elseif($type == 2)      // 30 derniers jours
        {
            $months = array();
            for ($i = 1; $i <= 30; $i++)
            {
                $months[] = date("M", strtotime( date( 'Y-m-d' )." -$i days"));
            }
            return $months;
        }
        elseif($type == 5)      // 90 derniers jours
        {
            $months = array();
            for ($i = 1; $i <= 90; $i++)
            {
                $months[] = date("M", strtotime( date( 'Y-m-d' )." -$i days"));
            }
            return $months;
        }

    }

    public function getTimesYears($type)
    {
        if($type == 1)      // 7 derniers jours
        {
            $years = array();
            for ($i = 1; $i <= 7; $i++)
            {
                $years[] = date("Y", strtotime( date( 'Y-m-d' )." -$i days"));
            }
            return $years;
        }
        elseif($type == 2)      // 30 derniers jours
        {
            $years = array();
            for ($i = 1; $i <= 30; $i++)
            {
                $years[] = date("Y", strtotime( date( 'Y-m-d' )." -$i days"));
            }
            return $years;
        }
        elseif($type == 5)      // 90 derniers jours
        {
            $years = array();
            for ($i = 1; $i <= 90; $i++)
            {
                $years[] = date("Y", strtotime( date( 'Y-m-d' )." -$i days"));
            }
            return $years;
        }
        elseif($type == 6)      // 6 derniers mois
        {
            $years = array();
            for ($i = 1; $i <= 6; $i++)
            {
                $years[] = date("Y", strtotime( date( 'Y-m-d' )." -$i months"));
            }
            return $years;
        }
        elseif($type == 3)      // 12 derniers mois
        {
            $years = array();
            for ($i = 1; $i <= 12; $i++)
            {
                $years[] = date("Y", strtotime( date( 'Y-m-d' )." -$i months"));
            }
            return $years;
        }
        elseif($type == 4)      // 24 derniers mois
        {
            $years = array();
            for ($i = 1; $i <= 24; $i++)
            {
                $years[] = date("Y", strtotime( date( 'Y-m-d' )." -$i months"));
            }
            return $years;
        }

    }
}
