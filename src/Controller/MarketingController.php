<?php

namespace App\Controller;

use App\Service\EmailMarketing;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

class MarketingController extends AbstractController
{
    /**
     * @Route("/marketing", name="marketing")
     * @param KernelInterface $kernel
     * @param EmailMarketing $emailMarketing
     * @return Response
     */
    public function index(KernelInterface $kernel, EmailMarketing $emailMarketing): Response
    {
        /*$application = new Application($kernel);
        $input = new ArrayInput([
            'command' => 'app:email-sending',
        ]);
        $output = new NullOutput();
        try {
            $application->run($input, $output);
        } catch (\Exception $e) {
        }*/
        $emailMarketing->send();

        return new Response("Sent");

        /*return $this->render('marketing/index.html.twig', [
            'controller_name' => 'MarketingController',
        ]);*/
    }
}
