<?php

namespace App\Controller;

use App\Entity\Offer;
use App\Entity\Order;
use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Form\RegistrationOrderFormType;
use App\Repository\OfferRepository;
use App\Service\EmailManager;
use App\Service\TrackManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SalesController extends AbstractController
{
    /**
     * @Route("/notre-offre", name="offer")
     * @param OfferRepository $offerRepository
     * @param TrackManager $trackManager
     * @return Response
     */
    public function index(
        OfferRepository $offerRepository,
        TrackManager $trackManager
    ): Response
    {
        $user = $this->getUser();
        if(
            ($user && !in_array('ROLE_ADMIN', $user->getRoles())) || !$user
        ) {
            $trackManager->new('sales', 'notre-offre', $user);
        }
        $offers = $offerRepository->findAll();

        return $this->render('sales/offer.html.twig', [
            'offers' => $offers
        ]);
    }

    /**
     * @Route("/order/{offer}", name="order")
     *
     * @param Offer $offer
     * @param Request $request
     * @param TrackManager $trackManager
     * @return Response
     */
    public function order(
        Offer $offer,
        Request $request,
        TrackManager $trackManager
    )
    : Response
    {
        $users = $request->get('users');
        $duration = $request->get('duration');
        $formView = null;
        $trackManager->new(
            'sales',
            'commande-'.$offer->getName().'-'.$users.'_users'.'-'.$duration.'_year',
            $this->getUser()
        );
        if(!$this->getUser())
        {
            $form = $this->createForm(RegistrationOrderFormType::class);
            $formView = $form->createView();
        }
        return $this->render('sales/order.html.twig', [
            'offer' => $offer,
            'users' => $users,
            'duration' => $duration,
            'registrationForm' => $formView
        ]);
    }

    /**
     * @Route("/order/{offer}/confirm", name="confirm_order")
     *
     * @param Offer $offer
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param EmailManager $emailManager
     * @param TrackManager $trackManager
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function confirmOrder(
        Offer $offer,
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        EmailManager $emailManager,
        TrackManager $trackManager,
        EntityManagerInterface $entityManager
    ): Response
    {
        if(!$this->getUser())
        {
            $customer = new User();
            $form = $this->createForm(RegistrationOrderFormType::class, $customer);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid())
            {
                $customer->setPassword(
                    $passwordEncoder->encodePassword(
                        $customer,
                        $form->get('plainPassword')->getData()
                    )
                );
                $customer->setRoles(['ROLE_USER']);
                $customer->setType('customer');
                $entityManager->persist($customer);
            }
        }
        else
        {
            $customer = $this->getUser();
        }
        $users = $request->get('users');
        $duration = $request->get('duration');

        $order = new Order();
        $order->setOffer($offer);
        $order->setState('submitted');
        $order->setPrice($request->get('price'));
        $order->setDuration($duration);
        $order->setQuantity($users);
        $entityManager->persist($order);
        $customer->setType('customer');
        $order->setCustomer($customer);
        $entityManager->flush();

        $trackManager->new(
            'sales', 'commande-confirm-'.$offer->getName().'-'.$users.'_users'.'-'.$duration.'_year', $customer
        );
        $emailManager->send(
            'order-creation',
            'Nouvelle Commande Ajoutée - PACK '.$offer->getName(),
            $customer,
            $order
        );
        $emailManager->send(
            'order-confirmation',
            'A Propos de Votre Commande, '.$customer->getPseudo(),
            $customer,
            $order
        );

        return new Response(1);
    }
}
