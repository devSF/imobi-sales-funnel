<?php /** @noinspection DuplicatedCode */

namespace App\Controller;

use App\Entity\Sending;
use App\Entity\User;
use App\Repository\EmailRepository;
use App\Service\EmailManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Workflow\Registry;

class AdminController extends EasyAdminController
{
    private $entityManager;
    private $registry;
    private $emailManager;
    private $emailRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        Registry $registry,
        EmailManager $emailManager,
        EmailRepository $emailRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->registry = $registry;
        $this->emailManager = $emailManager;
        $this->emailRepository = $emailRepository;
    }

    /**
     * @Route("/prospect/review/{email}", name="review_prospect")
     *
     * @param Request $request
     * @param User $prospect
     * @return Response
     */
    public function reviewProspect(
        Request $request,
        User $prospect
    ): Response
    {
        $accepted = !$request->query->get('reject');
        $machine = $this->registry->get($prospect);

        if (
            $machine->can($prospect, 'accept')
            ||
            $machine->can($prospect, 'reject')
        ) {
            $transition = $accepted ? 'accept' : 'reject';
        }
        else {
            return new Response('Prospect déjà validé ou en état érroné.');
        }
        $machine->apply($prospect, $transition);

        if($accepted)
        {
            $prospect->setIsEnabled(true);

            $sending = new Sending();
            $sending->setUser($prospect);
            $sending->setEmail($this->emailRepository->find(1));
            $sending->setSentAt(
                (new \DateTime())->add(new \DateInterval('P3D'))
            );
            $this->entityManager->persist($sending);
        }

        $this->entityManager->flush();

        /*** SENDING EMAILS HERE***/

        $this->emailManager->send(
            'prospect-validation',
            'A Propos de Votre Demande de Démo, '.$prospect->getPseudo(),
            $prospect
        );

        return $this->render('admin/prospect_review.html.twig', [
            'transition' => $transition,
            'prospect' => $prospect,
        ]);
    }

    public function updateUserEntity($user)
    {
        /* Put your code here */

        $machine = $this->registry->get($user);
        if (
            $machine->can($user, 'accept')
            ||
            $machine->can($user, 'reject')
        ) {
            $accepted = $user->isEnabled();
            $transition = $accepted ? 'accept' : 'reject';
            $machine->apply($user, $transition);

            if($accepted)
            {
                $user->setIsEnabled(true);

                $sending = new Sending();
                $sending->setUser($user);
                $sending->setEmail($this->emailRepository->find(1));
                $sending->setSentAt(
                    (new \DateTime())->add(new \DateInterval('P3D'))
                );
                $this->entityManager->persist($sending);
            }

            /*** SENDING EMAILS HERE***/
            $this->emailManager->send(
                'prospect-validation',
                'A Propos de Votre Demande de Démo, '.$user->getPseudo(),
                $user
            );

            $this->entityManager->flush();
        }

        parent::updateEntity($user);
    }

    public function removeOrderEntity($order)
    {
        if($customer = $order->getCustomer()) {
            $customer->setType('prospect');
            $machine = $this->registry->get($customer);

            if($machine->can($customer, 'remove_order')) {
                $machine->apply($customer, 'remove_order');
            }

            $this->entityManager->flush();
        }

        parent::removeEntity($order);

    }
}
