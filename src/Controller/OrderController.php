<?php

namespace App\Controller;

use App\Entity\Order;
use App\Form\OrderAccessType;
use App\Service\EmailManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Workflow\Registry;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class OrderController extends AbstractController
{
    private $entityManager;
    private $registry;
    private $emailManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        Registry $registry,
        EmailManager $emailManager
    )
    {
        $this->entityManager = $entityManager;
        $this->registry = $registry;
        $this->emailManager = $emailManager;
    }

    /**
     * @Route("/create-access", name="create_access")
     * @param Request $request
     * @param Session $session
     * @param EmailManager $emailManager
     * @return Response
     */
    public function createAccess(
        Request $request,
        Session $session,
        EmailManager $emailManager
    ): Response
    {
        $repository = $this->entityManager->getRepository(Order::class);
        $id = $request->query->get('id');
        $order = $repository->find($id);

        $form = $this->createForm(OrderAccessType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $machine = $this->registry->get($order);

            if ($machine->can($order, 'create'))
            {
                $customer = $order->getCustomer();
                $machine->apply($order, 'create');
                try {
                    $emailManager->send(
                        'order-validation',
                        'Commande #'.$order->getId().' - Accés Logiciel',
                        $customer,
                        $order
                    );
                    $customer->setIsEnabled(true);
                    $this->entityManager->flush();
                }
                catch (TransportExceptionInterface $e) {}
            }
            else {
                return new Response('Accés déjà créé.');
            }

            $session->getFlashBag()->add(
                'success',
                'Commande #'.$order->getId().' mise à jour.'
            );

            return $this->redirectToRoute('easyadmin', [
                'action' => 'list',
                'entity' => 'Order',
            ]);
        }

        return $this->render('admin/order_access.html.twig', [
            'order' => $order,
            'orderForm' => $form->createView(),
        ]);
    }
}
