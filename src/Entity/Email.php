<?php

namespace App\Entity;

use App\Repository\EmailRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmailRepository::class)
 */
class Email
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subject;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $signature;

    /**
     * @ORM\OneToMany(targetEntity=Sending::class, mappedBy="email", orphanRemoval=true)
     */
    private $sendings;

    /**
     * @ORM\OneToOne(targetEntity=Email::class, cascade={"persist", "remove"})
     */
    private $next;

    public function __construct()
    {
        $this->sendings = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->subject;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getSignature(): ?string
    {
        return $this->signature;
    }

    public function setSignature(?string $signature): self
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * @return Collection|Sending[]
     */
    public function getSendings(): Collection
    {
        return $this->sendings;
    }

    public function addSending(Sending $sending): self
    {
        if (!$this->sendings->contains($sending)) {
            $this->sendings[] = $sending;
            $sending->setEmail($this);
        }

        return $this;
    }

    public function removeSending(Sending $sending): self
    {
        if ($this->sendings->removeElement($sending)) {
            // set the owning side to null (unless already changed)
            if ($sending->getEmail() === $this) {
                $sending->setEmail(null);
            }
        }

        return $this;
    }

    public function getNext(): ?self
    {
        return $this->next;
    }

    public function setNext(?self $next): self
    {
        $this->next = $next;

        return $this;
    }
}
