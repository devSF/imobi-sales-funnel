<?php


namespace App\Notification;


use App\Entity\User;
use Symfony\Component\Notifier\Message\EmailMessage;
use Symfony\Component\Notifier\Notification\EmailNotificationInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\EmailRecipientInterface;

class ProspectReviewNotification extends Notification implements EmailNotificationInterface
{

    private $prospect;

    public function __construct(User $prospect, $subject)
    {
        $this->prospect = $prospect;
        $this->subject($subject);
    }

    public function asEmailMessage(EmailRecipientInterface $recipient, string $transport = null): ?EmailMessage
    {
        $message = EmailMessage::fromNotification($this, $recipient);
        $message
            ->getMessage()
            ->htmlTemplate('emails/prospect_review_notification.html.twig')
            ->context([
                'prospect' => $this->prospect,
                'footer_text' => 'Notification e-mail envoyé par Imobi.'
            ])
        ;
        return $message;
    }
}