<?php


namespace App\Notification;


use App\Entity\Order;
use App\Entity\User;
use Symfony\Component\Notifier\Message\EmailMessage;
use Symfony\Component\Notifier\Notification\EmailNotificationInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\EmailRecipientInterface;

class OrderConfirmationNotification extends Notification implements EmailNotificationInterface
{

    private $customer;
    private $order;

    public function __construct(User $customer, Order $order, $subject)
    {
        $this->customer = $customer;
        $this->order = $order;
        $this->subject($subject);
    }

    public function asEmailMessage(EmailRecipientInterface $recipient, string $transport = null): ?EmailMessage
    {
        $message = EmailMessage::fromNotification($this, $recipient);
        $message
            ->getMessage()
            ->htmlTemplate('emails/order_confirmation_notification.html.twig')
            ->context([
                'customer' => $this->customer,
                'order' => $this->order,
                'footer_text' => 'Notification e-mail envoyé par Imobi.'
            ])
        ;
        return $message;
    }
}