<?php

namespace App\Form;

use App\Entity\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderAccessType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('access', null, [
                    'label'  => 'Lien Web*',
                    'attr' => [
                        'class' => 'form-control mb-3'
                    ],
                ]
            )
            ->add('username', null, [
                    'label'  => 'Nom Utilisateur*',
                    'attr' => [
                        'class' => 'form-control mb-3'
                    ],
                ]
            )
            ->add('password', null, [
                    'label'  => 'Mot de Passe Temporaire*',
                    'attr' => [
                        'class' => 'form-control mb-3'
                    ],
                ]
            )
            ->add('submit', SubmitType::class, [
                'label' => "Confirmer",
                'attr' => [
                    'class' => 'btn btn-lg btn-primary mt-5'
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }
}
